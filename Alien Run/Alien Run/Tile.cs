﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;


namespace Alien_Run
{
    class Tile
    {

        // enum
        public enum TileType
        {
            IMPASSIBLE, // = 0 Blocks movement from all directions
            PLATFORM, // = 1 Blocks downward movement
            SPIKE, // = 2 Hazard to player
            GOAL, // = 3 Goal for player
            POINTS // = 4 player points

        }

        // Data
        Texture2D spr;
        Vector2 pos;
        private TileType type;
        private int points = 0;
        private bool visible = true;

        public Tile(Texture2D newSpr, Vector2 newPos, TileType newType, int newPoints = 0)
        {
            spr = newSpr;
            pos = newPos;
            type = newType;
            points = newPoints;
        }

        // Behaviour
        public void LoadContnet(ContentManager content)
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
            {
                spriteBatch.Draw(spr, pos, Color.White);
            }
        }

        public Rectangle GetBounds()
        {
            return new Rectangle((int)pos.X, (int)pos.Y, spr.Width, spr.Height);
        }

        public TileType GetTileType()
        {
            return type;
        }

        public int GetPoints()
        {
            return points;
        }

        public void Hide()
        {
            visible = false;
        }

        public bool GetVisable()
        {
            return visible;
        }

    }
}
