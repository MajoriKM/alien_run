﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
namespace Alien_Run
{
    class Player
    {
        //data
        Texture2D spr = null;
        Vector2 position = Vector2.Zero;
        Vector2 velocity = Vector2.Zero;
        private Vector2 prevPos = Vector2.Zero;

        private Level ourLevel = null;
        private bool touchingGround = false;
        private float jumpButtonTime = 0f;
        private bool JumpLaunchInProgress = false;
        private int score = 0;

        //constant
        const float MOVE_SPD = 300f;
        const float GRAV_ACCEL = 3400f;
        const float TERMINAL_VOL = 550f;
        const float JUMP_LAUNCH_VEL = -1500.0f;
        const float MAX_JUMP_TIME = 0.3f;
        const float JUMP_CONTROL_POWER = 0.9f;

        //behaviours

        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }


        public void LoadContent(ContentManager content)
        {
            spr = content.Load<Texture2D>("player/player-stand");
        }

        public void Update(GameTime gameTime)
        {

            if (ourLevel.GetCompletedLevel() == true)
            {
                return;
            }

            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // update velocity based on input, gravity, etc
            // horizontaal velocity is always constant

            velocity.X = MOVE_SPD;

            // apply acceleration due to gravity
            velocity.Y += GRAV_ACCEL * deltaTime;

            // clamp vertical velocity to a terminal range
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VOL, TERMINAL_VOL);
            input(gameTime);

            prevPos = position;
            position += velocity * deltaTime;

            // velocity.X += spd;

            //check for collision
            CheckTileCollision();


        }

        private void CheckTileCollision()
        {
            // start assuming ground is not touching
            touchingGround = false;


            //use players bounding box to get list of tiles being collided with
            Rectangle bounds = GetBounds();
            Rectangle prevPlayerBounds = GetPrevBounds();

            List<Tile> collidingTiles = ourLevel.GetTilesInBounds(bounds);

            foreach(Tile collidingTile in collidingTiles)
            {
                Rectangle tileBounds = collidingTile.GetBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, bounds);
                Tile.TileType tileType = collidingTile.GetTileType();

                if(depth != Vector2.Zero)
                {
                    if(tileType == Tile.TileType.SPIKE)
                    {

                        KillPlayer();
                        return;
                    }
                    else if (tileType == Tile.TileType.GOAL)
                    {
                        ourLevel.CompletLevel();
                        return;
                    }
                    else if (tileType == Tile.TileType.POINTS)
                    {
                        // add points to score based on type of coin
                        score += collidingTile.GetPoints();
                        // hide coin
                        collidingTile.Hide();

                        continue;
                    }


                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);

                    if (absDepthY < absDepthX)
                    {
                        bool fallingOnTile = bounds.Bottom > tileBounds.Top && prevPlayerBounds.Bottom <= tileBounds.Top;

                        if (tileType == Tile.TileType.IMPASSIBLE || (tileType == Tile.TileType.PLATFORM && fallingOnTile))
                        {
                            position.Y += depth.Y;

                            bounds = GetBounds();
                            if (bounds.Bottom >= tileBounds.Top)
                            {
                                touchingGround = true;
                            }
                        }
                        //only if feet are below ground should touching ground be assumed
                        
                    }
                    else if(tileType == Tile.TileType.IMPASSIBLE)
                    {
                        position.X += depth.X;

                        bounds = GetBounds();
                    }
                }
            }
        }

        private Rectangle GetBounds()
        {
            return new Rectangle((int)position.X,(int)position.Y,spr.Width,spr.Height);
        }

        private Rectangle GetPrevBounds()
        {
            return new Rectangle((int)prevPos.X, (int)prevPos.Y, spr.Width, spr.Height);
        }

        public void Draw(SpriteBatch spritebatch)
        {

            spritebatch.Draw(spr, position, Color.White);
           
        }

        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            // this calculates how far our rectangles are overlapping

            //calculate half sizes of both rects
            float halfWidthPlayer = player.Width / 2;
            float halfHeightPlayer = player.Height / 2;
            float halfWidthTile = tile.Width / 2;
            float halfHeightTile = tile.Height / 2;

            Vector2 centrePlayer = new Vector2(player.Left + halfWidthPlayer, player.Top + halfHeightPlayer);

            Vector2 centreTile = new Vector2(tile.Left + halfWidthTile, tile.Top + halfHeightTile);

            //how far apart are the centres of the rect from eachother
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //min distance these need to not collide
            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfHeightTile;

            // If we are not intersecting return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // calculate and return intersection depth
            float depthX, depthY;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }

        private void input(GameTime gameTime)
        {
            KeyboardState keystate = Keyboard.GetState();

            bool allowedToJump = touchingGround == true || 
                (JumpLaunchInProgress == true && jumpButtonTime <= MAX_JUMP_TIME);

            if (keystate.IsKeyDown(Keys.Space) && allowedToJump == true)
            {

                //record jumping
                JumpLaunchInProgress = true;
                jumpButtonTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

                //trying to jump
                //(Too simple)
                //velocity.Y = JUMP_LAUNCH_VEL;


                // scale launch velocity based on how long space is pressed
                velocity.Y = JUMP_LAUNCH_VEL * (1.0f - (float)Math.Pow(jumpButtonTime / MAX_JUMP_TIME, JUMP_CONTROL_POWER));

            }
            else
            {
                JumpLaunchInProgress = false;
                jumpButtonTime = 0;
            }
        
        }

        public Vector2 GetPosition()
        {
            return position;
        }

        private void KillPlayer()
        {
            ourLevel.SetupLevel();
            position = Vector2.Zero;
            prevPos = Vector2.Zero;
            velocity = Vector2.Zero;
            JumpLaunchInProgress = false;
            score = 0;
        }

        public int GetScore()
        {
            return score;
        }

    }
}
