﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Alien_Run
{
    class Level
    {

        // Data

        Tile[,] tiles;

        private Texture2D boxTexture;
        private Texture2D bridgeTexture;
        private Texture2D spikeTexture;
        private Texture2D flagTexture;
        private Texture2D bronzeCoinTexture;
        private Texture2D silverCoinTexture;
        private Texture2D goldCoinTexture;
        private bool completedLevel = false;
        //Texture2D tile_Box;

        const int LEVEL_WIDTH = 100;
        const int LEVEL_HEIGHT = 100;
        const int TILE_WIDTH = 70;
        const int TILE_HEIGHT = 70;


        // Behaviour
        public void LoadContent(ContentManager content)
        {
            // creating single tile texture for all tiles
            boxTexture = content.Load<Texture2D>("tiles/box");
            bridgeTexture = content.Load<Texture2D>("tiles/bridge");
            spikeTexture = content.Load<Texture2D>("tiles/spikes");
            flagTexture = content.Load<Texture2D>("tiles/goal");
            bronzeCoinTexture = content.Load<Texture2D>("items/coinBronze");
            silverCoinTexture = content.Load<Texture2D>("items/coinSilver");
            goldCoinTexture = content.Load<Texture2D>("items/coinGold");

            SetupLevel();
        }

        public void SetupLevel()
        {
            completedLevel = false;

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            //create box
            CreateBox(0, 8);
            CreateBox(1, 8);
            CreateBox(2, 8);
            CreateBox(3, 8);
            CreateBox(4, 8);
            CreateBox(5, 8);
            CreateBox(6, 8);
            CreateBox(7, 8);
            CreateBox(8, 8);
            CreateBox(9, 8);
            CreateBox(10, 8);
            CreateBox(11, 8);
            CreateBox(12, 8);
            CreateBox(14, 8);
            CreateBox(15, 8);
            CreateBox(16, 8);
            CreateBox(17, 8);
            CreateBox(18, 8);

            // create platforms
            CreateBridge(1, 6);
            CreateBridge(2, 6);
            CreateBridge(6, 6);
            CreateBridge(7, 6);
            CreateBridge(8, 6);
            CreateBridge(9, 6);

            // coins
            CreateBronzeCoin(1, 5);
            CreateBronzeCoin(2, 5);
            CreateSilverCoin(6, 5);
            CreateSilverCoin(7, 5);
            CreateGoldCoin(8, 5);



            //create Spikes
            
            CreateSpikes(8, 7);
            CreateSpikes(9, 7);
            CreateSpikes(10, 7);


            // goal
            CreateGoal(18, 7);
        }

        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(boxTexture, tilePosition, Tile.TileType.IMPASSIBLE);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(bridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateSpikes(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(spikeTexture, tilePosition, Tile.TileType.SPIKE);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateGoal(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(flagTexture, tilePosition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateBronzeCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(bronzeCoinTexture, tilePosition, Tile.TileType.POINTS, 1);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateSilverCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(silverCoinTexture, tilePosition, Tile.TileType.POINTS, 5);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateGoldCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(goldCoinTexture, tilePosition, Tile.TileType.POINTS, 10);
            tiles[tileX, tileY] = newTile;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for(int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for(int y = 0; y <LEVEL_HEIGHT; ++y)
                {
                    if(tiles[x,y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }

        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            List<Tile> tilesInBounds = new List<Tile>();

            // determine tile ccord range for the rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottumTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGHT) - 1;

            //loop through range and add tiles to list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottumTile; ++y)
                {
                    // only add tile if exist, not null and visible
                    Tile thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisable() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }

        public Tile GetTile (int x, int y)
        {
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;

            return tiles[x, y];
        }

        public void CompletLevel()
        {
            completedLevel = true;
        }

        public bool GetCompletedLevel()
        {
            return completedLevel;
        }


    }
}
